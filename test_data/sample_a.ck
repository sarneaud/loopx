110.0 => float bpm;
60::second / bpm / 4 => dur sixteenth;
4 * sixteenth => dur quarter;

// Midi notes for I, vi, ii, V chord progression
[
	[0, 4, 7], // C major
	[9, 12, 16], // A minor
	[2, 5, 9], // D minor
	[7, 11, 14] // G major
]
@=> int chords[][];
[0, 1, 2, 1, 0, 1, 2, 1] @=> int arpeggio[];

Envelope output_env_l => BPF output_bpf_l => dac.left;
Envelope output_env_r => BPF output_bpf_r => dac.right;
// 20Hz-20kHz band-pass filter
output_bpf_l.set(632.455, 0.03165);
output_bpf_r.set(632.455, 0.03165);
Pan2 output;
output.left => output_env_l;
output.right => output_env_r;

dac => WvOut2 wav_out => blackhole;
"sample_a.wav" => wav_out.wavFilename;

false => int is_done;

fun void playArpeggio()
{
	PercFlut arp => JCRev arp_r => Pan2 pan => output;
	0.5 => pan.pan;
	for (0 => int ch; ch < chords.size(); ch++)
	{
		for (0 => int j; j < arpeggio.size(); j++)
		{
			Std.mtof(48 + chords[ch][arpeggio[j]]) => arp.freq;
			0.5 => arp.noteOn;
			sixteenth => now;
			0.5 => arp.noteOff;
			sixteenth => now;
		}
	}
}

fun void playBellLoop()
{
	TubeBell bell => JCRev bell_r => output;
	while (!is_done)
	{
		for (0 => int ch; ch < chords.size(); ch++)
		{
			Std.mtof(72 + chords[ch][0]) => bell.freq;
			0.2 => bell.noteOn;
			1 * quarter => now;
			0.1 => bell.noteOff;
			3 * quarter => now;
		}
	}
}

fun void playShakerLoop()
{
	Shakers shakers => Pan2 pan => output;
	-0.5 => pan.pan;
	5 => shakers.preset;
	while (!is_done)
	{
		1.5 => shakers.noteOn;
		4 * quarter => now;
	}
	1.0 => shakers.noteOff;
}

fun void playBeatLoop()
{
	Shakers perc => Pan2 p => output;
	-0.8 => p.pan;
	11 => perc.preset;
	while (!is_done)
	{
		2.0 => perc.noteOn;
		quarter => now;
	}
}

0.0 => output_env_l.value => output_env_r.value;
3::second => output_env_l.duration => output_env_r.duration;
output_env_l.keyOn();
output_env_r.keyOn();

spork ~ playShakerLoop();
spork ~ playBeatLoop();
spork ~ playBellLoop();

playArpeggio();
playArpeggio();

6::second => output_env_l.duration => output_env_r.duration;
output_env_l.keyOff();
output_env_r.keyOff();
playArpeggio();

true => is_done;
