# `loopx`

A tool for automatically creating high-quality extended versions of looped music.

There's [an explanation of how it works on my blog](https://theartofmachinery.com/2021/03/12/loopx.html).

## Installation

I've only tested it on GNU/Linux systems.

`loopx` requires [FFTW](http://www.fftw.org/) and [Libsndfile](http://libsndfile.github.io/libsndfile/).  You can probably get them from your package manager with a command similar to this:

```
sudo apt-get install libsndfile-dev fftw3-dev
```

If you want to build your own copy of `loopx` from source, you'll need a [D language](https://dlang.org/) compiler and the [`dub`](https://dub.pm/) build system.  You might also find these in your package manager, but they might be outdated versions.  If so, you can [download official up-to-date copies](https://dlang.org/download.html).  `loopx` is known to build with DMD v2.094.1 and `dub` v1.23.0.

With that all ready, you can build like this:

```
dub build --build=release
```

## Usage

The input audio files should loop the music a few times (three times is recommended, but twice often works).  It's okay if the files contain an intro or fade in/out as long as there is enough cleanly looped audio.

Quick example:
```
loopx -v 30m '01 intro.ogg' '30 game over.ogg'
```
(Creates two files, `01 intro-30m.ogg` and `30 game over-30m.ogg`, each as close as possible to 30 minutes in length.)

All options:
```
Usage: loopx [--help] [--version] [--output-dir=<string>] [--verbose]
             [--quiet] duration [input files...]

Positional arguments:
 duration        target duration. Can include unit suffixes s (seconds), m (minutes) or h
                 (hours). E.g., 1m20s. Without units, final number assumed to be
                 seconds.
 input files     paths of audio files to extend

Optional arguments:
 --help, -h      display this help and exit
 --version       display version and exit
 --output-dir, --od <string>
                 output directory
 --verbose, -v   verbose output
 --quiet, -q     quiet output
```

## Audio formats

All audio decoding/encoding is handled by Libsndfile, so the supported audio formats are whatever is supported by your copy of Libsndfile.  That should be most popular formats with free implementations, except MP3.  If you're wondering about MP3 support, check the status of [this ticket for adding MP3 support to Libsndfile](https://github.com/libsndfile/libsndfile/issues/258).

`loopx` splices together unmodified samples from the input file to construct the output file.  If you're using a lossless codec, such as FLAC, the output will be a faithful reproduction of the input, except (of course) at the splice points where the music is looped back.

## "It doesn't work!"

I'm releasing this software because it works for me and I hope other people will get use out of it (check the license).  If it doesn't work for you, you might have to loop your music the old-fashioned way by hand.
