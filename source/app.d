import std.conv;
import std.exception;
import std.file;
import logger = std.experimental.logger;
import std.path;
import std.string : toStringz;
import std.stdio;

import darg;

import loopx;
import loopx.sndfile;
import loopx.util;

struct Options
{
	@Option("help", "h")
	@Help("display this help and exit")
	OptionFlag help;

	@Option("version")
	@Help("display version and exit")
	OptionFlag show_version;

	@Option("output-dir", "od")
	@Help("output directory")
	string out_dir = ".";

	@Option("verbose", "v")
	@Help("verbose output")
	OptionFlag is_verbose;

	@Option("quiet", "q")
	@Help("quiet output")
	OptionFlag is_quiet;

	@Argument("duration")
	@Help("target duration. Can include unit suffixes s (seconds), m (minutes) or h (hours).  E.g., 1m20s.  Without units, final number assumed to be seconds.")
	string target_duration_spec;

	@Argument("input files", Multiplicity.zeroOrMore)
	@Help("paths of audio files to extend")
	string[] fnames;
}

int main(string[] args)
{
	if (args.length > 1 && args[1] == "--version")
	{
		writeln(loopx_version);
		return 0;
	}

	const usage = usageString!Options(args[0]);
	Options options;
	try
	{
		options = parseArgs!Options(args[1..$]);

		enforce(!(options.is_verbose && options.is_quiet), "Can't set both verbose and quiet output");

		if (options.show_version)
		{
			writeln(loopx_version);
			return 0;
		}
	}
	catch (ArgParseHelp e)
	{
		writeln(usage);
		writeln(helpString!Options());
		return 0;
	}
	catch (Exception e)
	{
		writeln(e.msg);
		writeln(usage);
		return 1;
	}

	with (logger)
	{
		const level = options.is_verbose ? LogLevel.trace : options.is_quiet ? LogLevel.warning : LogLevel.info;
		sharedLog = new LoopXLogger(stderr, level);
	}

	try
	{
		foreach (fname; options.fnames)
		{
			processFile(fname, dirName(fname), options);
		}
	}
	catch (LoopXException e)
	{
		logger.error(e.msg);
		return 1;
	}
	return 0;
}

void processFile(string fname, string src_path, scope ref const(Options) options)
{
	try
	{
		logger.infof("Extending '%s'", fname);
		SFInfo info;
		auto sf_input = sf_open(fname.toStringz, SFMode.read, &info);
		scope (exit) sf_close(sf_input);

		const data = loadData(sf_input, info);

		const plan = makeExtensionPlan(data, &info, options.target_duration_spec);
		info.num_frames = numFrames(plan, info.num_channels);

		auto out_fname = outFileName(fname, src_path, options);
		mkdirRecurse(dirName(out_fname));
		auto sf_output = sf_open(out_fname.toStringz, SFMode.write, &info);
		scope (exit) sf_close(sf_output);

		sf_input.copyTagsTo(sf_output);

		try
		{
			executePlan(plan, sf_input, sf_output, &info);
		}
		catch (LoopXException e)
		{
			throw new LoopXException(text("Error writing output file '", out_fname, "': ", e.msg));
		}
	}
	catch (LoopXException e)
	{
		throw new LoopXException(text("Error processing '", fname, "': ", e.msg));
	}
}

string outFileName(string in_file_name, string src_path, scope ref const(Options) options) pure
{
	auto fname = relativePath(in_file_name, src_path);
	fname = text(stripExtension(fname), "-", options.target_duration_spec, extension(fname));
	return buildPath(options.out_dir, fname);
}

unittest
{
	Options options;
	options.duration = "1m";
	assert (outFileName("foo.ogg", ".", options) == "./foo-1m.ogg");
	assert (outFileName("foo", ".", options) == "./foo-1m");
	assert (outFileName("dir/foo.ogg", ".", options) == "./dir/foo-1m.ogg");
	assert (outFileName("dir/foo.ogg", "dir", options) == "./foo-1m.ogg");
	assert (outFileName("dir1/dir2/dir3/foo.ogg", "dir1", options) == "./dir2/dir3/foo-1m.ogg");

	options.out_dir = "out";
	assert (outFileName("foo.ogg", ".", options) == "out/foo-1m.ogg");
	assert (outFileName("foo", ".", options) == "out/foo-1m");
	assert (outFileName("dir/foo.ogg", ".", options) == "out/dir/foo-1m.ogg");
	assert (outFileName("dir/foo.ogg", "dir", options) == "out/foo-1m.ogg");
	assert (outFileName("dir1/dir2/dir3/foo.ogg", "dir1", options) == "out/dir2/dir3/foo-1m.ogg");
}

class LoopXLogger : logger.Logger
{
	this (File output, logger.LogLevel level) @safe
	{
		_output = output;
		super(level);
	}

	override void writeLogMsg(ref LogEntry payload)
	{
		with (payload)
		{
			_output.writef("%s [%s] %s\n", timestamp, logLevel, msg);
		}
	}

	private:
	File _output;
}
