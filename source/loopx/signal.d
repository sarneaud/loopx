module loopx.signal;

import std.algorithm;
import std.conv : to;
import std.exception;
import std.math;
import std.range;
import std.stdio;
import std.typecons;

import loopx.fftw;
import loopx.util;

void makeHann(double[] x) pure
{
	foreach (j; 0..x.length)
	{
		const s = sin(PI * j / x.length);
		x[j] = s * s;
	}
}

double toMelScale(double f) pure
{
	return 1127 * log(1 + f/700);
}

double fromMelScale(double m) pure
{
	return 700 * (exp(m/1127) - 1);
}

unittest
{
	foreach (f; iota(10.0, 20_000.0, 10.0))
	{
		assert (approxEqual(fromMelScale(toMelScale(f)), f));
	}
}

size_t[] calcMelScaledIdxs(size_t fnum, double fmax, size_t num_idxs) pure
in
{
	assert (num_idxs > 3);
	assert (fmax >= 20000);
}
do
{
	const m_lo = toMelScale(20);
	const m_hi = toMelScale(20000);
	const m_d = (m_hi - m_lo) / (num_idxs - 3);
	size_t jthIdx(size_t j) pure
	{
		const m = m_lo + j * m_d;
		const f = fromMelScale(m);
		return cast(size_t)(floor(f / fmax * (fnum - 1)));
	}
	return chain(only(size_t(0)), iota(num_idxs-2).map!jthIdx, only(fnum-1)).array;
}

enum WindowOverlap
{
	half,
	maximum,
}

struct SpectrumAnalyser
{
	@disable this();
	@disable this(ref SpectrumAnalyser);

	this(uint sample_rate, uint num_channels)
	{
		_sample_rate = sample_rate;
		_num_channels = num_channels;

		const w_length = max(128, roundUpPower2(sample_rate * 3 / 40));

		auto window = makeFftwArray!double(w_length);
		makeHann(window);
		_window = window;

		_fkey_idxs = calcMelScaledIdxs(w_length/2 + 1, 0.5 * sample_rate, 8 * 12 + 2);
	}

	~this()
	{
		freeFftwArray(_window);
	}

	SpectrogramRange analyseAll(return scope ref VectorAlloc alloc, return scope const(double)[] data, WindowOverlap overlap) return scope
	{
		const s = spacing(overlap);
		return SpectrogramRange(alloc, &this, data, _window, s);
	}

	size_t spacing(WindowOverlap overlap) const pure
	{
		with (WindowOverlap) final switch (overlap)
		{
			case half:
				return _window.length / 2;
			case maximum:
				return 1;
		}
	}

	void analyse(scope const(double)[] data, scope ref FftBuffer fft, scope double[] output) const
	in
	{
		assert (fft.buffer.length == _window.length);
		assert (data.length >= _num_channels * _window.length);
		assert (output.length >= frame_length);
	}
	do
	{
		const channel_length = _fkey_idxs.length - 2;
		foreach (ch; 0.._num_channels)
		{
			copy(data[ch..$].stride(_num_channels).take(_window.length), fft.buffer);
			fft.buffer[] *= _window[];
			fft.fft();
			copy(fft.xbuffer.map!(z => (z * z.conj).re), fft.buffer);
			makeSpectrogramFrame(fft.buffer[0..fft.xbuffer.length], _fkey_idxs, output[0..channel_length]);
			output = output[channel_length..$];
		}
	}

	size_t min_data() const pure
	{
		return _num_channels * _window.length;
	}

	uint sample_rate() const pure
	{
		return _sample_rate;
	}

	uint num_channels() const pure
	{
		return _num_channels;
	}

	size_t msToSamples(ulong ms) const pure
	{
		return (ms * _sample_rate * _num_channels / 1000).to!size_t;
	}

	const(double)[] window() const pure
	{
		return _window;
	}

	const(size_t)[] fkey_idxs() const pure
	{
		return _fkey_idxs;
	}

	size_t frame_length() const pure
	{
		return _num_channels * (_fkey_idxs.length - 2);
	}

	private:
	const(double)[] _window;
	const(size_t)[] _fkey_idxs;
	uint _sample_rate, _num_channels;
}

struct SpectrogramRange
{
	@disable this();
	@disable this(ref SpectrogramRange);

	this(return scope ref VectorAlloc alloc, return scope SpectrumAnalyser* analyser, return scope const(double)[] data, const(double)[] window, size_t spacing)
	{
		_analyser = analyser;
		_data = data;
		_spacing = spacing;

		_front = alloc.make!double(analyser.num_channels * (analyser.fkey_idxs.length - 2));

		_fft = FftBuffer(alloc, analyser.window.length, FftPlanStrategy.measure, No.make_inverse);
		calcFront();
	}

	bool empty() const pure
	{
		return _data.length < _analyser.window.length * _analyser.num_channels;
	}

	const(double)[] front() const pure
	{
		return _front;
	}

	void popFront()
	{
		_data = _data[_spacing * _analyser.num_channels..$];
		if (!empty) calcFront();
	}

	uint num_channels() const pure
	{
		return _analyser.num_channels;
	}

	size_t spacing() const pure
	{
		return _spacing;
	}

	size_t frame_length() const pure
	{
		return _analyser.frame_length;
	}

	size_t numFramesToEnd() const pure
	{
		if (empty) return 0;
		with (_analyser) return (_data.length - window.length * num_channels) / (_spacing * num_channels) + 1;
	}

	size_t numSamplesToEnd() const pure
	{
		return _data.length;
	}

	private:

	void calcFront()
	{
		_analyser.analyse(_data, _fft, _front);
	}

	SpectrumAnalyser* _analyser;
	const(double)[] _data;
	double[] _front;
	FftBuffer _fft;
	size_t _spacing;
}

auto toMatrix(ref SpectrogramRange sp, size_t num_frames)
{
	auto buffer = new double[](num_frames * sp.frame_length);
	auto ret = buffer.chunks(sp.frame_length);
	foreach (f; 0..num_frames)
	{
		assert(!sp.empty);
		copy(sp.front, ret[f++]);
		sp.popFront();
	}
	return ret;
}

void makeSpectrogramFrame(scope const(double)[] dft_sq, scope const(size_t)[] fkey_idxs, scope double[] output) pure
in
{
	assert (fkey_idxs.length == output.length + 2);
}
out
{
	assert (!output.any!isNaN);
}
do
{
	foreach (j; 1..fkey_idxs.length-1)
	{
		double acc = 0.0;
		const prev = fkey_idxs[j-1], cur = fkey_idxs[j], next = fkey_idxs[j+1];
		foreach (k; prev..cur)
		{
			acc += dft_sq[k] * (k-prev+1) / (cur-prev);
		}
		foreach (k; cur..next)
		{
			acc += dft_sq[k] * (next-k) / (next-cur);
		}

		output[j-1] = log(1 + acc) - log(1);
	}
}

double spectrogramFrameDiff(scope const(double)[] a, scope const(double)[] b)
in
{
	assert (!a.any!isNaN);
	assert (!b.any!isNaN);
}
do
{
	return zip(a, b).map!(t => abs(t[0] - t[1])).sum;
}
