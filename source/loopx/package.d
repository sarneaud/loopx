module loopx;

import core.time;
import std.algorithm;
import std.array;
import std.conv;
import std.exception;
import logger = std.experimental.logger;
import std.parallelism;
import std.range;
import std.typecons;

import loopx.fftw;
import loopx.signal;
import loopx.sndfile;
import loopx.util;

enum min_period_s = 4;
enum padding_ms = 1500;
enum max_duration_s = 1000_000;
static assert (min_period_s * 1000 > padding_ms);

alias CopySpec = Tuple!(size_t, "start", size_t, "length", uint, "num_repeats");

/// Goes from raw decoded floating point data from a sound file to a final plan for copying data to a target_duration_spec length
CopySpec[] makeExtensionPlan(scope const(double)[] data, scope const(SFInfo)* input_info, string target_duration_spec)
{
	const sample_rate = input_info.sample_rate;
	const num_channels = input_info.num_channels;

	enforce!LoopXException(data.length > min_period_s * sample_rate * num_channels * 2, text("Music sample must be at least 2 repetitions of a loop of at least ", min_period_s, " seconds"));

	auto analyser = SpectrumAnalyser(sample_rate, num_channels);
	assert (analyser.min_data < min_period_s * sample_rate);

	auto period = estimatePeriod(data, sample_rate, num_channels);
	logger.tracef("Initial period estimate: %s", toTimeString(period, input_info));

	const padding = analyser.msToSamples(padding_ms);
	const loop_zone = findLoopZone(data, period, analyser, padding, analyser.min_data);
	logger.tracef("Working with loop zone from %s to %s", toTimeString(loop_zone.start, input_info), toTimeString(loop_zone.start+loop_zone.length, input_info));

	period = refinePeriod(data, period, loop_zone, analyser);
	logger.tracef("Refined period estimate: %s", toTimeString(period, input_info));

	const duration_limit_s = min(max_duration_s, size_t.max/sample_rate/num_channels/2);
	const target_duration = evaluateDurationSpec(target_duration_spec);
	enforce!LoopXException(target_duration < dur!"seconds"(duration_limit_s), text("Target duration of ", target_duration, " is longer than ", duration_limit_s, " seconds"));
	const target_samples = target_duration.total!"seconds" * sample_rate * num_channels;

	auto plan = planOutputCopies(data.length, loop_zone, period, target_samples);
	plan = makePlanSeamless(plan, data, analyser);

	return plan;
}

long numFrames(scope const(CopySpec)[] plan, uint num_channels) pure
{
	return plan.map!(s => s.length * s.num_repeats).sum * num_channels;
}

void executePlan(scope const(CopySpec)[] plan, scope SndFile* sf_input, scope SndFile* sf_output, scope const(SFInfo)* input_info)
{
	if (input_info.isFloatFormat())
	{
		copyFileData!"double"(sf_input, sf_output, plan, input_info);
	}
	else
	{
		copyFileData!"int"(sf_input, sf_output, plan, input_info);
	}
}

private:

size_t estimatePeriod(scope const(double)[] data, uint sample_rate, uint num_channels)
in (num_channels > 0)
{
	// Finds the sum of the autocorrelations of each channel (calculated with FFT), normalises and finds a peak to estimate the original period

	logger.trace("Estimating period");
	const channel_length = data.length / num_channels;
	// Discrete Fourier Transforms are inherently cyclical
	// We want to treat the audio data like a slice of an infinite timeline,
	// but with the FFT, any data shifted out one end gets rotated to the other.
	// A workaround is padding the vector with enough zeros that we can ignore edge effects.
	// Normally the padded length would be double the original to make it impossible for rotated
	// data to overlap with the original.  In this case we're assuming the music period is no more
	// than half the total length, so we don't care if correlation values for shifts longer than
	// that get borked.  That means we can get away with 1.5x padded length, instead of 2x.
	//
	// With many FFT implementations, rounding up to the next power of two length would make things
	// faster, but FFTW seems to be good enough that increasing the length hurts as much as it helps.
	const vec_length = (channel_length * 3 + 1) / 2;

	// TODO: make sure there's nothing like false sharing slowing this down
	VectorAlloc vec_alloc;
	scope ffts = new FftBuffer[](num_channels);
	foreach (ch; taskPool.parallel(iota(num_channels)))
	{
		ffts[ch] = FftBuffer(vec_alloc, vec_length);
		copy(data[ch..$].stride(num_channels), ffts[ch].buffer[0..channel_length]);
		ffts[ch].buffer[channel_length..$][] = 0.0;
		ffts[ch].fft();
		foreach (ref c; ffts[ch].xbuffer) c *= c.conj;
	}
	foreach (j; 1..num_channels)
	{
		ffts[0].xbuffer[] += ffts[j].xbuffer[];
	}
	ffts[0].ifft();

	double evaluatePeriod(size_t shift)
	{
		// The autocorrelation is normalised using the overlap length, plus a bit to bias towards shorter shifts
		// (multiples of the period will also have peaks)
		return ffts[0].buffer[shift] / (channel_length - shift + shift/8);
	}

	// Assume period is greater than min_period_s
	// Also, if the music loop is played at least twice in the sample, the period will be no more than half the total data length
	const skip = min_period_s * sample_rate;
	return iota(skip, (channel_length+1)/2).maxElement!evaluatePeriod * num_channels;
}

alias TimeRange = Tuple!(size_t, "start", size_t, "length");

TimeRange findLoopZone(scope const(double)[] data, size_t period, scope ref SpectrumAnalyser analyser, size_t padding, size_t min_analyser_data)
in
{
	assert (padding > 0);
	assert (padding*2 < period);
	assert (data.length >= 2*period);
	assert (data.length > period*3/2 + 2*padding + min_analyser_data);
}
out (ret)
{
	assert (ret.start >= padding);
	assert (ret.length > period);
	assert (ret.start + ret.length + padding + min_analyser_data <= data.length);
}
do
{
	// The data gets turned into a spectrogram, then deltas are calculated between each frame and the one a period length after
	// The loop zone will be the region of the data with low delta values

	logger.trace("Finding loop zone");

	const padded_end = data.length - padding - min_analyser_data;
	assert (padded_end > padding);  // True from contract

	const deltas = periodShiftDeltas(data, period, analyser, WindowOverlap.half);
	const samples_per_spectrogram_frame = analyser.spacing(WindowOverlap.half) * analyser.num_channels;
	const scaled_window_length = period / samples_per_spectrogram_frame / 2;

	// The thresholds for "low" and "high" delta are estimated by looking at the lowest-error region of a fixed window size
	const scaled_padding = 1 + (padding-1) / samples_per_spectrogram_frame;
	const scaled_padded_end = (padded_end-period) / samples_per_spectrogram_frame;
	double threshold_ok = double.infinity, threshold_ideal = double.infinity;
	double sse = 0, best_sse = double.infinity;
	size_t best_window_idx = scaled_padding;
	foreach (wi; scaled_padding..scaled_padded_end-scaled_window_length)
	{
		// FIXME this whole loop calculation seems to do the job but could be much less stupid
		static double sq(double x) { return x * x; }
		const window = deltas[wi..wi+scaled_window_length];
		sse += sq(window[$-1]) - sq(window[0]);
		if (sse < best_sse)
		{
			threshold_ok = window.reduce!max;
			threshold_ideal = sum(window) / window.length;
			best_sse = sse;
			best_window_idx = wi;
		}
	}

	// Using the thresholds, we find the loop zone in the spectrogram, convert to original sample units, and adjust as needed

	auto ret = schmittLongestRange(deltas, threshold_ok, threshold_ideal);
	ret.start *= samples_per_spectrogram_frame;
	ret.length = ret.length * samples_per_spectrogram_frame + period;

	if (ret.start < padding)
	{
		ret.length -= padding - ret.start;
		ret.start = padding;
	}
	if (ret.start + ret.length > padded_end)
	{
		assert (ret.start < padded_end);
		ret.length = padded_end - ret.start;
	}
	if (ret.length <= period)
	{
		logger.warning("Failed to find a clean loop zone. The result might not sound good.");
		ret.start = best_window_idx * samples_per_spectrogram_frame;
		ret.length = period + period/2;
	}

	return ret;
}

/// Calculates deltas between each spectrogram frame and the one a period length later
const(double[]) periodShiftDeltas(scope const(double)[] data, size_t period, scope ref SpectrumAnalyser analyser, WindowOverlap spec_overlap)
{
	VectorAlloc vec_alloc;
	scope spgen1 = analyser.analyseAll(vec_alloc, data, spec_overlap);
	scope spgen2 = analyser.analyseAll(vec_alloc, data[period..$], spec_overlap);
	auto deltas_app = appender!(double[]);
	deltas_app.reserve(spgen2.numFramesToEnd());
	while(!spgen2.empty)
	{
		const delta = spectrogramFrameDiff(spgen1.front, spgen2.front);
		deltas_app.put(delta);
		spgen1.popFront();
		spgen2.popFront();
	}

	// The deltas can be very noisy
	// A median filter makes the loop zone more obvious
	static double median3(R)(R v)
	{
		if ((v[0] < v[1]) == (v[1] < v[2])) return v[1];
		if ((v[1] < v[0]) == (v[0] < v[2])) return v[0];
		return v[2];
	}
	return chain(only(deltas_app[][0]), deltas_app[].slide(3).map!median3, only(deltas_app[][$-1])).array;
}

TimeRange schmittLongestRange(const(double)[] deltas, double threshold_ok, double threshold_ideal)
{
	// This uses a longest range that meets the threshold constraints, according to a variant of Schmitt triggering
	// This variant is conservative
	// When we hit the end of a threshold_ok zone, the range is trimmed back to the last place it met threshold_ideal
	size_t ok_start = 0, last_ideal = 0;
	bool is_ok = true;
	TimeRange ret;

	foreach (idx, delta; deltas)
	{
		if (is_ok)
		{
			if (delta > threshold_ok)
			{
				is_ok = false;

				if (last_ideal - ok_start > ret.length)
				{
					ret = TimeRange(ok_start, last_ideal - ok_start);
				}
			}
			else if (delta <= threshold_ideal)
			{
				last_ideal = idx;
			}
		}
		else
		{
			if (delta <= threshold_ideal)
			{
				is_ok = true;
				ok_start = last_ideal = idx;
			}
		}
	}

	return ret;
}

size_t refinePeriod(scope const(double)[] data, size_t period, TimeRange loop_zone, scope ref SpectrumAnalyser analyser)
{
	logger.trace("Refining period estimate");

	// Any feature of the music will repeat exactly one period later
	// An initial period estimate can be refined by matching features at one point with the corresponding repeats one period later

	const scan_width = analyser.window.length * analyser.num_channels;
	assert (scan_width < analyser.msToSamples(padding_ms));
	assert (period > scan_width + analyser.window.length * analyser.num_channels);
	assert (loop_zone.length > period);

	const interest_zone = TimeRange(loop_zone.start, loop_zone.length - period);

	// High novelty points are used to help matching
	const interest_points = findInterestPoints(data, interest_zone, analyser);
	if (interest_points.empty) return period;

	// std.parallelism.amap would be perfect here if it weren't for https://issues.dlang.org/show_bug.cgi?id=5711
	auto corrected_periods = new size_t[](interest_points.length);
	foreach (idx, interest_point; taskPool.parallel(interest_points))
	{
		corrected_periods[idx] = estimateCorrectedPeriodAtPoint(data, interest_point, period, analyser, scan_width);
	}

	static size_t median(size_t[] a) pure
	{
		sort(a);  // This is good enough because the number of points is small
		if ((a.length & 1) == 1) return a[a.length/2];
		const v1 = a[a.length/2];
		const v2 = a[a.length/2+1];
		return v1 + (v2 - v1)/2;
	}
	return median(corrected_periods[]);
}

size_t[] findInterestPoints(scope const(double)[] data, TimeRange zone, scope ref SpectrumAnalyser analyser)
in
{
	assert (zone.start + zone.length <= data.length);
}
do
{
	VectorAlloc vec_alloc;
	scope prev_frame = vec_alloc.make!double(analyser.frame_length);

	// A novelty curve spikes at changes in music
	// This is negative so that we can reuse leastIndexes() to find the highest novelty areas
	double negNovelty(size_t idx)
	{
		scope spgen = analyser.analyseAll(vec_alloc, data[idx..$], WindowOverlap.half);  // FIXME
		// FIXME explain
		assert (spgen.numFramesToEnd() >= 2);
		copy(spgen.front, prev_frame);
		spgen.popFront();

		return -spectrogramFrameDiff(prev_frame, spgen.front);
	}

	const num_channels = analyser.num_channels;
	auto idxs = QuasirandomSequence(zone.length/num_channels).map!(i => zone.start + i*num_channels).take(1000);
	return idxs.leastIndexes!(5, negNovelty)[].map!(p => p.idx).array;
}

size_t estimateCorrectedPeriodAtPoint(scope const(double)[] data, size_t interest_point, size_t period, scope ref SpectrumAnalyser analyser, size_t scan_width)
{
	VectorAlloc vec_alloc;

	// Uses cross-correlation of the spectrogram around interest_point with interest_point+period to find the best shift that makes the two match up
	// This shift is used as a correction to the period estimate

	scope spgen1 = analyser.analyseAll(vec_alloc, data[interest_point-scan_width/4..$], WindowOverlap.maximum);
	auto spectrogram1 = spgen1.toMatrix(scan_width/2);

	scope spgen2 = analyser.analyseAll(vec_alloc, data[period+interest_point-scan_width/2..$], WindowOverlap.maximum);
	auto spectrogram2 = spgen2.toMatrix(scan_width);

	scope fft = FftBuffer(vec_alloc, scan_width);
	scope xbuffer2 = vec_alloc.make!ComplexD(fft.xbuffer.length);
	scope cross_cor = vec_alloc.make!double(scan_width);
	cross_cor[] = 0.0;
	foreach (mel; 0..analyser.frame_length)
	{
		fft.buffer[] = 0.0;
		// retro effectively turns the correlation into a convolution, so the transforms can be multiplied directly without taking the conjugate
		copy(spectrogram1.transversal(mel).retro, fft.buffer[scan_width/4..$]);
		fft.fft();
		xbuffer2[] = fft.xbuffer[];

		copy(spectrogram2.transversal(mel), fft.buffer);
		fft.fft();

		// No conj here thanks to the retro above
		fft.xbuffer[] *= xbuffer2[];
		fft.ifft();

		cross_cor[] += fft.buffer[];
	}

	double best_value = -double.infinity;
	ptrdiff_t best_shift;
	foreach (j; iota(0, scan_width/4, analyser.num_channels))
	{
		if (cross_cor[j] > best_value)
		{
			best_shift = j;
			best_value = cross_cor[j];
		}
	}
	foreach (j; iota(scan_width - scan_width/4, scan_width, analyser.num_channels))
	{
		if (cross_cor[j] > best_value)
		{
			best_shift = j - scan_width;
			best_value = cross_cor[j];
		}
	}

	return period + best_shift;
}

CopySpec[] planOutputCopies(size_t full_length, ref const(TimeRange) loop_zone, size_t period, size_t target_samples)
{
	logger.trace("Planning data copies");

	// The basic idea is to copy as much as possible from the start until the end of the loop zone, loop back as many periods as possible, repeat a few times, then copy through to the end
	// There are some special cases, though
	// We might need to reduce the length of the track, and sometimes we can use fewer copy segments

	const loop_zone_end = loop_zone.start + loop_zone.length;
	const intro_outro_length = loop_zone.start + (full_length - loop_zone_end);

	if (target_samples < intro_outro_length)
	{
		logger.warning("Extending target duration to account for intro/outro");
		target_samples = intro_outro_length;
	}

	const target_loop_zone_length = target_samples - intro_outro_length;
	const num_extra_samples = target_loop_zone_length.to!ptrdiff_t - loop_zone.length.to!ptrdiff_t;
	int periodsRounded(ptrdiff_t samples)
	{
		const s = samples < 0 ? -1 : 1;
		const ps = period.to!ptrdiff_t;
		return ((samples + s * ps/2) / ps).to!int;
	}
	const num_extra_cycles = periodsRounded(num_extra_samples);
	if (num_extra_cycles == 0) return [CopySpec(0, full_length, 1)];
	if (num_extra_cycles < 0)
	{
		return [CopySpec(0, loop_zone_end + period * num_extra_cycles, 1), CopySpec(loop_zone_end, full_length - loop_zone_end, 1)];
	}

	const num_full_loop_cycles = (loop_zone.length / period).to!uint;
	const num_big_rewinds = num_extra_cycles / num_full_loop_cycles;
	const num_small_rewind_cycles = num_extra_cycles - num_big_rewinds * num_full_loop_cycles;

	const big_rewind_start = loop_zone_end - num_full_loop_cycles * period;
	const last_rewind_start = loop_zone_end - num_small_rewind_cycles * period;

	auto ret = [CopySpec(0, loop_zone_end, 1)];
	if (num_small_rewind_cycles == 0)
	{
		ret ~= CopySpec(big_rewind_start, num_full_loop_cycles * period, num_big_rewinds-1),
		ret ~= CopySpec(big_rewind_start, full_length - big_rewind_start, 1);
	}
	else
	{
		const small_rewind_start = loop_zone_end - num_small_rewind_cycles * period;
		ret ~= CopySpec(big_rewind_start, num_full_loop_cycles * period, num_big_rewinds),
		ret ~= CopySpec(small_rewind_start, full_length - small_rewind_start, 1);
	}
	return ret;
}

CopySpec[] makePlanSeamless(CopySpec[] plan, scope const(double)[] data, scope ref SpectrumAnalyser analyser)
in
{
	foreach (j; 1..plan.length) assert(plan[j].start >= analyser.msToSamples(padding_ms));
	foreach (j; 0..plan.length-1) assert(plan[j].start + plan[j].length <= data.length - analyser.msToSamples(padding_ms));
	// If the last plan had repeats, we'd need extra code to make that splice seamless
	assert (plan.length < 2 || plan[$-1].num_repeats == 1);
}
do
{
	if (plan.length < 2) return plan;

	// This is the amount we're willing to adjust the split point, searching for a better match
	// It's a made-up number based loosely on this study of real music timing accuracy:
	// https://musicmachinery.com/2009/03/02/in-search-of-the-click-track/
	const uint search_delta = analyser.sample_rate / 200 * analyser.num_channels;

	VectorAlloc vec_alloc;
	scope fft = FftBuffer(vec_alloc, analyser.window.length);
	// This is the sample radius used to evaluate match quality
	const match_delta = fft.buffer.length * analyser.num_channels / 2;
	assert (match_delta % analyser.num_channels == 0);  // True if fft.buffer.length is even. (It's meant to be a power of 2.)
	assert (match_delta <= analyser.msToSamples(padding_ms));
	scope match_data = vec_alloc.make!double(2*match_delta);
	scope spec_ideal = vec_alloc.make!double(analyser.frame_length);
	scope spec_spliced = vec_alloc.make!double(analyser.frame_length);

	double spliceScore(size_t a_mid, size_t b_mid)
	{
		copy(data[a_mid-match_delta..a_mid], match_data);
		copy(data[b_mid..b_mid+match_delta], match_data[match_delta..$]);
		analyser.analyse(match_data, fft, spec_spliced);
		return spectrogramFrameDiff(spec_ideal, spec_spliced);
	}

	auto searchRange(size_t mid)
	in
	{
		assert (mid >= match_delta);
		assert (data.length - mid >= match_delta);
	}
	do
	{
		const neg_delta = min(search_delta, mid - match_delta);
		const pos_delta = min(search_delta, data.length - mid - match_delta);
		return iota(mid - neg_delta, mid + pos_delta, analyser.num_channels);
	}

	// The best plan is found using Dijkstra's algorithm
	// Instead of min-total cost, this search finds min-max distortion

	import std.container.binaryheap;

	static struct PlanNode
	{
		double distortion;
		immutable(PlanNode)* parent;
		size_t next_spec_idx;
		size_t length, next_start;
	}

	auto nodes_storage = [new immutable(PlanNode)(double.infinity, null, 0, 0, plan[0].start)];
	nodes_storage.reserve(1000);  // FIXME
	auto nodes = heapify!"a.distortion > b.distortion"(nodes_storage);  // min heap
	while (true)
	{
		assert (!nodes.empty); // FIXME
		auto n = nodes.front;
		nodes.popFront();

		const j = n.next_spec_idx;

		if (j+1 == plan.length)
		{
			foreach_reverse(idx; 1..plan.length)
			{
				plan[idx].start = n.next_start;
				plan[idx-1].length = n.length;
				n = n.parent;
			}
			plan[$-1].length = min(plan[$-1].length, data.length - plan[$-1].start);
			break;
		}

		// "left" and "right" refer to the left and right sides of the splice point
		// The left is the end of the earlier segment, and the right is the start of the next segment
		// Optimal splice points will be searched for around these initial points
		const init_left = min(data.length - match_delta, n.next_start + plan[j].length);
		const init_right = plan[j+1].start;

		enum kNCandidates = 3;
		StackArray!(IdxValue, kNCandidates) lefts;

		if (plan[j].num_repeats == 1)
		{
			auto energy = (size_t idx) => data[idx..idx+analyser.num_channels].map!(x => x * x).sum;
			lefts = searchRange(init_left).leastIndexes!(kNCandidates, energy);
			// Energy is just a heuristic for finding good fit
			// It isn't distortion
			foreach (ref l; lefts[]) l.value = -double.infinity;
		}
		else
		{
			copy(data[init_left-match_delta..init_left+match_delta], match_data);
			analyser.analyse(match_data, fft, spec_ideal);
			auto repeat_fit = (size_t idx) => spliceScore(idx, n.next_start);
			lefts = searchRange(init_left).leastIndexes!(kNCandidates, repeat_fit);
		}

		foreach (left; lefts[])
		{
			copy(data[left.idx-match_delta..left.idx+match_delta], match_data);
			analyser.analyse(match_data, fft, spec_ideal);
			auto splice_fit = (size_t idx) => spliceScore(left.idx, idx);
			if (j+1 < plan.length-1)
			{
				auto rights = searchRange(init_right).leastIndexes!(kNCandidates, splice_fit);
				foreach (right; rights[])
				{
					const next_start = right.idx;
					auto nn = new immutable(PlanNode)(max(n.distortion, left.value, right.value), n, n.next_spec_idx+1, left.idx - n.next_start, next_start);
					// With Dijkstra's, it's often a good idea to check if the node has already been added with a lower value before adding it again
					// There's not much point here because the paths are short, and because of how min-max makes it behave
					nodes.insert(nn);
				}
			}
			else
			{
				const right = searchRange(init_right).map!(idx => IdxValue(splice_fit(idx), idx)).minElement!(p => p.value);
				const next_start = right.idx;
				auto nn = new immutable(PlanNode)(max(n.distortion, left.value, right.value), n, n.next_spec_idx+1, left.idx - n.next_start, next_start);
				nodes.insert(nn);
			}
		}
	}

	return plan;
}

void copyFileData(string datatype)(scope SndFile* input, scope SndFile* output, scope const(CopySpec)[] plan, scope const(SFInfo)* info) if (datatype == "double" || datatype == "int")
{
	import std.string : fromStringz;
	logger.trace("Writing output file");
	auto buffer = new mixin(datatype)[](64 * 1024);
	size_t total_samples_written = 0;
	foreach (spec; plan)
	{
		logger.tracef(total_samples_written > 0, "Splice point at %s in output", toTimeString(total_samples_written, info));
		logger.tracef("Copying from %s to %s in input %d time(s)", toTimeString(spec.start, info), toTimeString(spec.start+spec.length, info), spec.num_repeats);
		logger.tracef(spec.num_repeats > 1, "Splice point at %s in output", toTimeString(total_samples_written + spec.length, info));
		foreach (r; 0..spec.num_repeats)
		{
			sf_seek(input, spec.start/info.num_channels, SFSeekWhence.set);
			size_t length_remaining = spec.length;
			while (length_remaining)
			{
				auto copy_length = min(buffer.length, length_remaining);
				const num_read = mixin("sf_read_", datatype, "(input, buffer.ptr, copy_length)");
				if (num_read != copy_length)
				{
					if (sf_error(input) == SFErr.no_error)
					{
						logger.warning("Input file reads don't add up and the output is probably distorted.  This may be caused by inaccurate seeking in a buggy version of libsndfile (e.g., https://github.com/libsndfile/libsndfile/issues/643 for Ogg/Vorbis).  Try converting inputs files to a different format.");
						copy_length = num_read;
						length_remaining = num_read;
					}
					else
					{
						auto err_msg = sf_strerror(input);
						throw new LoopXException(text("Error reading sound data: ", fromStringz(err_msg)));
					}
				}
				const num_written = mixin("sf_write_", datatype, "(output, buffer.ptr, copy_length)");
				if (num_written != copy_length)
				{
					auto err_msg = sf_strerror(output);
					throw new LoopXException(text("Error writing sound data: ", fromStringz(err_msg)));
				}
				length_remaining -= copy_length;
				total_samples_written += copy_length;
			}
		}
	}
}

unittest
{
	import std.file;
	import std.json;
	import std.path;
	import std.string;
	import std.math;

	const data_name = "sample_a";

	const golden = parseJSON(readText(buildPath("test_data", data_name ~ ".json")));

	SFInfo info;
	auto sf_input = sf_open(buildPath("test_data", data_name ~ ".ogg").toStringz, SFMode.read, &info);
	scope (exit) sf_close(sf_input);

	const data = loadData(sf_input, info);

	const period_estimate = estimatePeriod(data, info.sample_rate, info.num_channels);
	const expected_period = golden.object["period"].integer;

	assert (fabs(cast(double)period_estimate - cast(double)expected_period) / expected_period < 0.0005);
}
