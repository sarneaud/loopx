module loopx.fftw;

import core.sync.mutex;
import std.conv;
import std.exception;
public import std.typecons : Flag, Yes, No;

import loopx.util;

T[] makeFftwArray(T)(size_t length) if (is(T == double) || is(T == ComplexD))
{
	auto mem = enforce!LoopXException(cast(T*)fftw_malloc(T.sizeof * length), "Failed to allocate array");
	return mem[0..length];
}

void freeFftwArray(T)(T[] a)
{
	fftw_free(cast(void*)a.ptr);
}

struct VectorAlloc
{
	import std.experimental.allocator.mallocator : AlignedMallocator;

	@disable this(ref VectorAlloc);

	T[] make(T)(size_t length) return scope @trusted
	{
		auto ret = makeFftwArray!T(length);
		_ptrs ~= ret.ptr;
		return ret;
	}

	~this() @trusted
	{
		foreach (p; _ptrs) fftw_free(p);
	}

	private:
	void*[] _ptrs;
}

struct FftBuffer
{
	this(return scope ref VectorAlloc alloc, size_t real_length, FftPlanStrategy plan_strategy = FftPlanStrategy.estimate, Flag!"make_inverse" make_inverse = Yes.make_inverse)
	{
		_buffer = alloc.make!double(real_length);
		_xbuffer = alloc.make!ComplexD(complexBufferLength(real_length));
		_fft_plan = FftPlan.makeForward(_buffer, _xbuffer, plan_strategy);
		if (make_inverse)
		{
			_ifft_plan = FftPlan.makeInverse(_buffer, _xbuffer, plan_strategy);
		}
	}

	inout(double)[] buffer() return scope pure inout
	{
		return _buffer;
	}

	inout(ComplexD)[] xbuffer() return scope pure inout
	{
		return _xbuffer;
	}

	void fft()
	{
		_fft_plan.execute();
	}

	void ifft()
	{
		_ifft_plan.execute();
	}

	static size_t complexBufferLength(size_t real_length) pure
	{
		return real_length/2 + 1;
	}

	private:
	double[] _buffer;
	ComplexD[] _xbuffer;
	FftPlan _fft_plan, _ifft_plan;
}

struct FftPlan
{
	@disable this(ref FftPlan);

	~this()
	{
		fftw_destroy_plan(_plan);
	}

	void execute()
	{
		fftw_execute(_plan);
	}

	static FftPlan makeForward(return scope double[] f, return scope ComplexD[] xf, FftPlanStrategy plan_strategy)
	in
	{
		assert (xf.length == FftBuffer.complexBufferLength(f.length));
	}
	do
	{
		fftw_planner_lock.lock_nothrow();
		scope (exit) fftw_planner_lock.unlock_nothrow();
		return FftPlan(fftw_plan_dft_r2c_1d(f.length.to!int, f.ptr, xf.ptr, plan_strategy));
	}

	static FftPlan makeInverse(return scope double[] f, return scope ComplexD[] xf, FftPlanStrategy plan_strategy)
	in
	{
		assert (xf.length == FftBuffer.complexBufferLength(f.length));
	}
	do
	{
		fftw_planner_lock.lock_nothrow();
		scope (exit) fftw_planner_lock.unlock_nothrow();
		return FftPlan(fftw_plan_dft_c2r_1d(f.length.to!int, xf.ptr, f.ptr, plan_strategy));
	}

	private:

	this(fftw_plan plan)
	{
		_plan = plan;
	}

	fftw_plan _plan;
}

enum FftPlanStrategy
{
	estimate = 1<<6, // "FFTW_ESTIMATE specifies that, instead of actual measurements of different algorithms, a simple heuristic is used to pick a (probably sub-optimal) plan quickly. With this flag, the input/output arrays are not overwritten during planning."
	measure = 0, // "FFTW_MEASURE tells FFTW to find an optimized plan by actually computing several FFTs and measuring their execution time. Depending on your machine, this can take some time (often a few seconds). FFTW_MEASURE is the default planning option."
}

private:

shared Mutex fftw_planner_lock;
shared static this()
{
	fftw_planner_lock = new shared Mutex();
}

extern (C):

alias fftw_plan = void*;

enum fftw_sign
{
	forward = -1,
	backward = 1,
}

void* fftw_malloc(size_t n);
void fftw_free(void*);
fftw_plan fftw_plan_dft_1d(int n, ComplexD* input, ComplexD* output, fftw_sign sign, uint flags);
fftw_plan fftw_plan_dft_r2c_1d(int n, double* input, ComplexD* output, uint flags);
fftw_plan fftw_plan_dft_c2r_1d(int n, ComplexD* input, double* output, uint flags);

void fftw_execute(const(fftw_plan));
void fftw_destroy_plan(const(fftw_plan));

void fftw_print_plan(const(fftw_plan));
