module loopx.sndfile;

import std.exception : enforce;

const(double)[] loadData(scope SndFile* sf, ref const(SFInfo) info)
{
	enum max_data_bytes = 1UL<<31;
	import std.conv : text;
	import loopx.util : LoopXException;
	// Dividing to avoid integer overflow
	enforce!LoopXException(max_data_bytes / double.sizeof / info.num_frames / info.num_channels >= 1, text("Audio file too large. Exceeds ", max_data_bytes, "B when decompressed."));

	const num_samples = info.num_frames * info.num_channels;
	auto ret = new double[num_samples];
	const num_read = sf_read_double(sf, ret.ptr, num_samples);

	if (num_read != num_samples)
	{
		import std.string : fromStringz;
		auto err_msg = sf_strerror(sf);
		throw new LoopXException(text("Error loading sound data: ", fromStringz(err_msg)));
	}

	return ret;
}

string toTimeString(ulong samples, scope const(SFInfo)* info)
{
	import std.format : format;
	const samples_per_second = info.num_channels * info.sample_rate;
	const minute = 60 * samples_per_second;
	const hour = 60 * minute;

	const num_hours = samples / hour;
	samples %= hour;
	const num_mins = samples / minute;
	samples %= minute;
	const num_secs = samples / samples_per_second;
	samples %= samples_per_second;
	const num_msecs = samples * 1000 / samples_per_second;

	return format("%02d:%02d:%02d.%03d", num_hours, num_mins, num_secs, num_msecs);
}

void copyTagsTo(SndFile* src, SndFile* dst)
{
	import std.string : toStringz, fromStringz;
	import std.traits : EnumMembers;
	static immutable all_tags = [EnumMembers!SFStr];
	foreach (tag; all_tags)
	{
		auto str = sf_get_string(src, tag);
		int err;
		if (tag == SFStr.software)
		{
			import loopx.util : loopx_version;
			if (str)
			{
				err = sf_set_string(dst, SFStr.software, (fromStringz(str) ~ " (loopx-" ~ loopx_version ~ ")").toStringz);
			}
			else
			{
				// If the file didn't already have this tag, ignore errors when trying to add it
				sf_set_string(dst, SFStr.software, ("loopx-" ~ loopx_version).ptr);
			}
		}
		else if (str && *str)
		{
			err = sf_set_string(dst, tag, str);
		}
		if (err)
		{
			import std.conv : text;
			import loopx.util : LoopXException;
			auto err_msg = sf_strerror(dst);
			throw new LoopXException(text("Error writing ", tag, " tag: ", fromStringz(err_msg)));
		}
	}
}

extern (C):

struct SndFile;

struct SFInfo
{
	ulong num_frames;
	uint sample_rate;
	uint num_channels;
	uint format;
	int sections;
	int seekable;

	bool isFloatFormat() const pure
	{
		enum SF_FORMAT_FLOAT = 0x0006;
		enum SF_FORMAT_DOUBLE = 0x0007;
		// https://github.com/libsndfile/libsndfile/issues/194
		enum SF_FORMAT_VORBIS = 0X0060;
		enum SF_FORMAT_OPUS = 0X0064;
		return (format & SF_FORMAT_FLOAT) || (format & SF_FORMAT_DOUBLE) || (format & SF_FORMAT_VORBIS) || (format & SF_FORMAT_OPUS);
	}
}

enum SFStr : int
{
	title = 0x01,
	copyright = 0x02,
	software = 0x03,
	artist = 0x04,
	comment = 0x05,
	date = 0x06,
	album = 0x07,
	license = 0x08,
	tracknumber = 0x09,
	genre = 0x10,
}

enum SFMode
{
	read = 0x10,
	write = 0x20,
	rdwr = 0x30,
}

enum SFErr : int
{
	no_error = 0,
	unrecognisedFormat = 1,
	system = 2,
	malformedFile = 3,
	unsupportedEncoding = 4
}

SFErr sf_error(SndFile*);
const(char)* sf_strerror(SndFile*);

SndFile* sf_open(const(char*) path, SFMode mode, SFInfo* sfinfo);
int sf_close(SndFile*);
void sf_write_sync(SndFile*);

long sf_read_double(SndFile*, double* ptr, long items);
long sf_write_double(SndFile*, const(double*) ptr, long items);

long sf_read_int(SndFile*, int* ptr, long items);
long sf_write_int(SndFile*, const(int*) ptr, long items);

const(char)* sf_get_string(SndFile*,SFStr);
int sf_set_string(SndFile*,SFStr,const(char)*);

private static import cstdio = core.stdc.stdio;

enum SFSeekWhence
{
	set = cstdio.SEEK_SET,
	cur = cstdio.SEEK_CUR,
	end = cstdio.SEEK_END,
}

long sf_seek(SndFile*, long frames, SFSeekWhence whence);
