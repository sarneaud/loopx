module loopx.util;

import core.time : dur, Duration;
import std.algorithm;
import std.exception;
import std.math;
import std.range;
import std.typecons;

immutable loopx_version = "0.2.1";

public import std.complex;
alias ComplexD = Complex!double;

class LoopXException : Exception
{
	mixin basicExceptionCtors;
}

alias IdxValue = Tuple!(double, "value", size_t, "idx");

StackArray!(IdxValue, N) leastIndexes(size_t N, alias eval, R)(auto ref R r)
{
	static assert (is(typeof(r.front) == size_t));
	static assert (is(typeof(eval(0)) == double));
	StackArray!(IdxValue,N) ret;
	ret.length = r.map!(i => IdxValue(eval(i), i)).topNCopy!"a < b"(ret[], Yes.sortOutput).length;
	return ret;
}

struct StackArray(E, size_t N)
{
	E[] opSlice() return
	{
		return _a[0..length];
	}

	size_t length = N;

private:
	E[N] _a;
}

unittest
{
	size_t[] v = [4, 7, 9, 3, 7, 3, 4, 1, 6, 7];
	static double sq(size_t x) pure
	{
		return x * x;
	}
	auto result1 = leastIndexes!(3, sq)(v);
	assert (result1.length == 3);
	assert (equal(result1[].map!(p => p.value), [1.0, 3*3, 3*3]));

	assert (v.length < 14);
	auto result2 = leastIndexes!(14, sq)(v);
	assert (result2.length == v.length);
	assert (equal(result2[].map!(p => p.idx).array.sort, v.sort));
	assert (result2[].map!(p => p.value).isSorted);
}

Duration evaluateDurationSpec(string spec)
{
	import std.utf : byCodeUnit;
	import std.ascii : isAlpha;
	import std.conv : to, ConvException;
	import std.functional : not;
	auto s = spec.byCodeUnit;
	Duration ret;
	while (!s.empty)
	{
		auto num_s = s.until!isAlpha;
		long value;
		try
		{
			value = num_s.to!long;
		}
		catch (ConvException e)
		{
			throw new LoopXException("Invalid duration '" ~ spec ~ "': " ~ e.msg);
		}
		s = s[num_s.walkLength..$];
		auto unit_s = s.until!(not!isAlpha);
		auto unit = s[0..unit_s.walkLength].source;
		switch (unit)
		{
			case "":
			case "s":
				ret += dur!"seconds"(value);
				break;

			case "m":
				ret += dur!"minutes"(value);
				break;

			case "h":
				ret += dur!"hours"(value);
				break;

			default:
				throw new LoopXException("Unrecognised unit for time duration: " ~ unit);
		}
		s = s[unit.length..$];
	}
	enforce(!ret.isNegative(), "Negative duration: " ~ spec);
	return ret;
}

unittest
{
	assert (evaluateDurationSpec("42") == dur!"seconds"(42));
	assert (evaluateDurationSpec("42s") == dur!"seconds"(42));
	assert (evaluateDurationSpec("42m") == dur!"minutes"(42));
	assert (evaluateDurationSpec("42h") == dur!"hours"(42));

	assert (evaluateDurationSpec("1h30m") == dur!"hours"(1) + dur!"minutes"(30));
	assert (evaluateDurationSpec("30s1m") == dur!"minutes"(1) + dur!"seconds"(30));
	assert (evaluateDurationSpec("1m10") == dur!"minutes"(1) + dur!"seconds"(10));

	assert (evaluateDurationSpec("1m-10s") == dur!"seconds"(50));
	assert (evaluateDurationSpec("-10s1m") == dur!"seconds"(50));

	assertThrown(evaluateDurationSpec("m"));
	assertThrown(evaluateDurationSpec("-1"));
	assertThrown(evaluateDurationSpec("60s-2m"));
}

struct QuasirandomSequence
{
	this(size_t length) pure
	{
		_length = length;
	}

	enum bool empty = false;

	size_t front() const pure
	{
		return cast(size_t)(_x * _length);
	}

	void popFront()
	{
		_x = fmod(_x + phi, 1.0);
	}

	private:
	size_t _length;
	double _x = phi;

	// Golden ratio
	enum phi = (sqrt(5.0) - 1) / 2;
}

unittest
{
	static assert (isInfinite!QuasirandomSequence);
	assert (equal(QuasirandomSequence(10).take(10), [6, 2, 8, 4, 0, 7, 3, 9, 5, 1]));
	assert (QuasirandomSequence(100).take(1000).all!(x => x < 100));
}

T roundUpPower2(T)(T v) pure if (is(const(T) == const(uint)) || is (const(T) == const(ulong)))
in (v != 0)
{
	import std.traits : Unconst;
	// https://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
	Unconst!T ret = v - 1;
	enum n = is(T == uint) ? 4 : 5;
	static foreach (j; 0..n)
	{
		ret |= ret >> (1<<j);
	}
	return ret + 1;
}

unittest
{
	static void test(T)()
	{
		assert (roundUpPower2(T(30000)) == T(32768));
		assert (roundUpPower2(T.max / 3) == T.max / 2 + 1);
		import std.conv : to;
		auto pwr = 0;
		foreach (v; T(1)..T(1000))
		{
			if (v > 1<<pwr) pwr++;
			assert (roundUpPower2(v) == 1<<pwr, v.to!string);
		}
	}
	test!uint();
	test!ulong();
}
